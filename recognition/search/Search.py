import re
import numpy as np
from os import listdir

import os.path as path
import time,pickle


def load_histograms(histograms_path):
    """
    Returns list of tuples (flag_file, histogram) that are loaded from given path
    :param histograms_path: path where histograms are stored
    """
    start = time.clock()
    histograms = []
    for dir_name in listdir(histograms_path):
        dir_path = path.join(histograms_path, dir_name)
        for file in listdir(dir_path):
            # load histogram and concatenate it in list
            histograms.append((file, np.load(path.join(dir_path, file))))
    end = time.clock()
    print("Histograms loaded in :", end - start, "sec")
    return histograms


def load_correlograms(correlograms_path):
    """
    Returns list of tuples (flag_file, histogram) that are loaded from given path
    :param histograms_path: path where histograms are stored
    """
    start = time.clock()
    correlograms = []
    for dir_name in listdir(correlograms_path):
        dir_path = path.join(correlograms_path, dir_name)
        for file in listdir(dir_path):
            # load correlogram object and concatenate it in list
            with open(path.join(dir_path, file), "rb") as sourc_file:
                correlograms.append((file, pickle.load(sourc_file)))

    end = time.clock()
    print("Correlograms loaded in :", end - start, "sec")
    return correlograms


class Search:
    def __init__(self, histograms):
        """
        :param histograms: List of tuples (fileName, fileHist)
        """
        self.histograms = histograms
        self.space_search_N = 200

    def distanceMethod(self, u, v):
        pass

    def searchKNN(self, histParam, N):
        """
        Returns N number
        :param histParam:
        :param N:
        :return:
        """
        start = time.clock()
        # calculate distances from given vector
        results = self.calculate_distances(histParam)
        # sort results
        results = sorted(results, key=lambda tup: tup[1])
        # take only first N country names
        country_names = self.get_most_similar_country(results)
        end = time.clock()
        print("Space search for flag elapsed: ", end - start, "sec")
        print()
        return country_names

    def get_most_similar_country(self, results):
        """
        Returns list of country names only.
        :param images: list of sorted result which contains tuples ( flag_file, distance)
        """
        # list contains (country name, distance). Cuts sufix which is produced genereting database
        countryname_distance = list(map(lambda t: (re.search(r'(.*)\d+_', t[0]).group(1), t[1]), results))

        # create a list of distnace values for certain flag
        country_values = {}
        for index, (country_name, dist) in enumerate(countryname_distance):
            tmp_list = country_values.get(country_name, [])
            tmp_list.append(dist)
            country_values[country_name] = tmp_list

        country_mean_dev_values = {}
        # calculate mean of distance list for certain country
        for country_name, values_list in country_values.items():
            country_mean_dev_values[country_name] = (np.mean(values_list), np.std(values_list))

        # unzip values
        sorted_results = self.sort_results(country_mean_dev_values)
        country_names, mean_dev = list(zip(*sorted_results))
        self.print_results(sorted_results, country_values)
        return country_names

    def sort_results(self, results):
        lis = results.items()
        mean_dev = np.mean([x[1][1] for x in lis])
        #print("Mean dev", mean_dev)
        sorted_lis = sorted(lis, key=lambda x: x[1][0])
        # return [x for x in sorted_lis if x[1][1] < mean_dev]
        return sorted_lis

    def calculate_distances(self, histConcatenated):
        """
        Return sorted list of distances between two vectors.
        :param histConcatenated:
        :return:
        """
        # initialize list of results
        # (fileName, histDistance)
        results = []

        for file_dataset, file_hist in self.histograms:
            dist = self.distanceMethod(histConcatenated, file_hist)
            # store the results
            results.append((file_dataset, dist))
        return results

    def print_results(self, country_mean_dev_values, country_values):
        for rez in sorted(country_mean_dev_values, key=lambda x: x[1][0], reverse=True):
            print(rez[0], " :", rez[1], "---", country_values[rez[0]])
        print("=======================================================")
