from recognition.search.Search import *


class HistogramSearch(Search):

    def distanceMethod(self, u, v):
        return scipy.spatial.distance.euclidean(u, v)
