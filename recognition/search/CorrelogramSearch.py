from recognition.search.Search import *


class CorrelogramSearch(Search):

    def distanceMethod(self, u, v):
        num_of_colors = u.numberOfBins ** 3
        distance = 0
        for k in u.D:
            for Ci in range(num_of_colors):
                for Cj in range(num_of_colors):
                    # one is added to avoid division with zero
                    distance += abs(u.colors_correlations[(Ci, Cj, k)] - v.colors_correlations[(Ci, Cj, k)])/ \
                                (1 + u.colors_correlations[(Ci, Cj, k)] + v.colors_correlations[(Ci, Cj, k)])
        return distance
