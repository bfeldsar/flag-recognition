import numpy as np
import cv2
from scipy import misc
from sys import path


class Histogram:

    def __init__(self, binsNum = 5, part_w = 100, part_h = 100):
        self.binsNum = binsNum
        self.part_w, self.part_h = part_w, part_h

    def calculate_descriptor(self, image):
        """
        Divides image on parts which are (width, height) = (part_w, part_h) and calculates their histograms then concatenates
        them
        :param image: image which histograms are calculated
        """
        img_w, img_h, img_ch = image.shape
        #divide image on subimages
        img_view = image.reshape(img_w // self.part_w, self.part_w, img_h // self.part_h, self.part_h, img_ch)
        subimages = np.swapaxes(img_view, 1, 2).reshape(-1, self.part_w, self.part_h, img_ch)

        # for each subimage create histogram and add it in list
        hists = []
        for img in subimages:
            hists.append(cv2.calcHist([img], [0, 1, 2], None, [self.binsNum, self.binsNum, self.binsNum], [0, 256, 0, 256, 0, 256]))
        # concatenate histograms as vector
        histConcatenated = np.asarray(hists).flatten().astype(np.float64, copy=True)

        #RootSIFT
        #source: https://www.robots.ox.ac.uk/~vgg/publications/2012/Arandjelovic12/arandjelovic12.pdf
        #normalize vector
        normalized_hist = np.empty(histConcatenated.shape)
        cv2.normalize(histConcatenated, normalized_hist,norm_type=cv2.NORM_L2)
        # calculate square root on each element
        squared_norm_hist = np.sqrt(normalized_hist)

        return squared_norm_hist


    def generate_descriptor(self, file_path, file_name, dest_folder):
        """
        Generates picture histograms, concatenate them and stores them in destination folder
        :param file_path: path where image is stored
        :param file_name: name of file
        :param dest_folder: path to folder where to store histogram
        :return:
        """
        #num = re.search(r'.*_(\d+).*',file_name).group(1)
        #if int(num) > 525:
        #    return
        image = misc.imread(file_path)
        histConcatenated = self.calculate_descriptor(image)
        # save histograms in file
        np.save(path.join(dest_folder, file_name), histConcatenated)
