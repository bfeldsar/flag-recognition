import numpy as np
import pickle, re


from scipy import misc
from os import path


class Correlogram:
    def __init__(self, numberOfBins=5, D=[1, 3, 5]):
        self.numberOfBins = numberOfBins
        self.D = D
        self.min_col = 1 / float(self.numberOfBins)

    def get_color_index(self, R, G, B):
        rgb_sum = R + G + B
        r = R / (0.01 + rgb_sum)
        g = G / (0.01 + rgb_sum)

        return self.get_index_in_chanel(r) + self.get_index_in_chanel(g) * self.numberOfBins

    def get_index_in_chanel(self, col):
        for i in range(self.numberOfBins):
            if col <= self.min_col * (i + 1):
                return i
        return -1

    def create_indexed_image(self, image):
        width, height, chan = image.shape
        indexed_img = np.empty((width, height), dtype=np.int64)
        # (key, value) -> (color, list of pixels)
        colors_map = {}
        for y in range(height):
            for x in range(width):
                r, g, b = image.item((x, y, 0)), image.item((x, y, 1)), image.item((x, y, 2))
                color = self.get_color_index(r, g, b)
                # set item in image
                indexed_img.itemset((x, y), color)
                # add pixel in colors map for given color
                pix_list = colors_map.get(color, set())
                pix_list.add((x, y))
                colors_map[color] = pix_list
        # change set to list in the color map
        for color, pix_list in colors_map.items():
            # take only 10 percent of pixels
            colors_map[color] = list(pix_list)[:len(pix_list) // 10]

        return indexed_img, colors_map

    def valid_pixel(self, image, pixel):
        """
        Returns True if pixel is inside the image boundaries.
        """
        return 0 <= pixel[0] < image.shape[0] and 0 <= pixel[1] < image.shape[1]

    def calculate_delta(self, indexed_img, C, k, pixel, h):
        """
        Returns sum of elements all over the distance k that has the same value as parameter C
        """
        x, y = pixel
        c_sum = 0
        v = 1 - h
        for i in range(0, k + 1):
            new_pixel = (x + h * i, y + v * i)
            if self.valid_pixel(indexed_img, new_pixel) and C == indexed_img.item(new_pixel):
                c_sum += 1
        return c_sum

    def color_correlation(self, indexed_img, colors_map, Ci, Cj, k):
        correl_sum = 0

        for x, y in colors_map.get(Ci, []):
            # this part of code returns sum of elements which create square with center in (x,y) and have the same value as parameter Cj
            correl_sum += self.calculate_delta(indexed_img, Cj, 2 * k, (x - k, y + k), 1) + \
                          self.calculate_delta(indexed_img, Cj, 2 * k, (x - k, y - k), 1) + \
                          self.calculate_delta(indexed_img, Cj, 2 * k - 2, (x - k, y - k + 1), 0) + \
                          self.calculate_delta(indexed_img, Cj, 2 * k - 2, (x + k, y - k + 1), 0)

        return correl_sum


    def calculate_descriptor(self, image):
        indexed_img, colors_map = self.create_indexed_image(image)
        descriptor = CorrelogramDescriptor(self.numberOfBins, self.D)
        num_of_colors = self.numberOfBins ** 2

        for k in self.D:
            for Ci in range(num_of_colors):
                for Cj in range(num_of_colors):
                    descriptor.colors_correlations[(Ci, Cj, k)] = self.color_correlation(indexed_img, colors_map, Ci,
                                                                                         Cj, k)

        return descriptor

    def generate_descriptor(self, file_path, file_name, dest_folder):
        """
        Generates picture histograms, concatenate them and stores them in destination folder
        :param file_path: path where image is stored
        :param file_name: name of file
        :param dest_folder: path to folder where to store histogram
        :return:
        """
        image = misc.imread(file_path)
        correlogram_descriptor = self.calculate_descriptor(image)
        # save correlogram in file
        with open(path.join(dest_folder, file_name), "wb") as file:
            pickle.dump(correlogram_descriptor, file)


class CorrelogramDescriptor:
    def __init__(self, numberOfBins, D):
        self.numberOfBins = numberOfBins
        self.D = D
        self.colors_correlations = {}
