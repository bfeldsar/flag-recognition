from os import listdir
from os import makedirs
from recognition.descriptor_creation.Correlogram import *
from recognition.descriptor_creation.Histogram import *

import os.path as path
import argparse
import time
import random

def init_parser():
    """
    Function initializes argument parser for command line call.
    :return: returns source path and destination path
    """
    parser = argparse.ArgumentParser(description='Database histogram creation')
    parser.add_argument('src', metavar='Source Path',
                        help='Path to the database directory.')
    parser.add_argument('dest', metavar='Destination Path',
                        help='Path to the directory where histograms of whole database will be created.')
    args = parser.parse_args()
    return args.src, args.dest


if __name__ == '__main__':
    start = time.clock()

    descriptor = Correlogram()
    # get source and destination path
    src_path, dest_path = init_parser()
    counter = 0
    # list folders in database
    for dir_name in listdir(src_path):
        dir_path = path.join(src_path, dir_name)

        if path.isdir(dir_path):
            print(counter,". Now creating descriptor for: ", dir_name)
            hist_folder = path.join(dest_path, dir_name)
            if not path.exists(hist_folder):
                makedirs(hist_folder)
            # list files in each folder
            for file_name in listdir(dir_path):
                file_path = path.join(dir_path, file_name)
                file_name_noext, file_ext = path.splitext(file_name)
                # check if it is file and its extension is jpeg
                if path.isfile(file_path) and file_ext == ".jpeg":
                    descriptor.generate_descriptor(file_path, file_name_noext, hist_folder)
            counter += 1
    end = time.clock()
    print("Descriptors database executed in ", end - start)
