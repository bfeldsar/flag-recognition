import argparse
import cv2
import os
from tkinter import *
from tkinter.colorchooser import *
from tkinter.filedialog import askopenfilename, asksaveasfilename

from PIL import Image, ImageTk
from functionalities import *

from database.flag_represent import *
from recognition.descriptor_creation.Correlogram import *
from recognition.search.Search import *
from recognition.search.CorrelogramSearch import *
from recognition.search.HistogramSearch import *

class GUI:
    def __init__(self, flag_db, hist_db):
        """

        :param flag_db:
        :param hist_db:
        """
        self.descriptor = Correlogram()
        self.color = "#000000"

        # canvas dimensions
        self.canvas_width = 500
        self.canvas_height = 300

        # toolbar icon dimensions
        self.icon_width = 50
        self.icon_height = 55

        # create empty opencv image and make it white
        self.opencv_image = np.zeros((self.canvas_height, self.canvas_width, 3), np.uint8)
        self.opencv_image.fill(255)
        self.initialize_image = self.opencv_image.copy()

        # initialize list of objects that are drawn on canvas
        self.objects_to_draw = []
        # list of undone objects
        self.undo_objects = []

        # initialize Search
        self.search = CorrelogramSearch(load_correlograms(hist_db))
        # initialize Represents
        self.represents = FlagRepresents(flag_db)

        self.project_dir = os.path.dirname(__file__)

        self.init_window()

    def init_window(self):
        """
        Function initialize main window
        """
        # create main window
        self.root = Tk()
        self.root.title("Flag canvas")
        self.root.resizable(0, 0)

        # crate two main frames
        toolbox_frame = Frame(self.root)
        toolbox_frame.pack(side=LEFT)

        right_frame = Frame(self.root)
        right_frame.pack(side=RIGHT)

        self.init_toolbox(toolbox_frame)
        self.init_canvas(right_frame)
        self.init_similarity_toolbar(right_frame)
        self.init_menu()

    def start(self):
        """
        Starts tkinter's main GUI loop. Shows window.
        """
        self.root.mainloop()

    def init_toolbox(self, toolbox_frame):
        """
        Initialize buttons on toolbox
        """
        self.radio_val = IntVar()

        # load images from file
        self.pencil_icon = PhotoImage(file=os.path.join(self.project_dir, "icons/pencil_icon.png"))
        self.line_icon = PhotoImage(file=os.path.join(self.project_dir, "icons/line_icon.png"))
        self.circle_icon = PhotoImage(file=os.path.join(self.project_dir, "icons/circle_icon.png"))
        self.fill_icon = PhotoImage(file=os.path.join(self.project_dir, "icons/fill_icon.png"))

        MODES = [
            (self.pencil_icon, self.create_pen),
            (self.line_icon, self.create_line),
            (self.circle_icon, self.create_circle),
            (self.fill_icon, self.create_fill)
        ]

        # thickness label
        thickness_label = Label(toolbox_frame, text="Tool thickness:")
        thickness_label.grid(row=0)

        # thickness scale
        self.thickness = Scale(toolbox_frame, from_=1, to=10, orient=HORIZONTAL)
        self.thickness.grid(row=1)

        # loop that creates and packs buttons
        for index, (icon, comm) in enumerate(MODES):
            button = Radiobutton(toolbox_frame, image=icon, width=self.icon_width, height=self.icon_height,
                                 command=comm, variable=self.radio_val, value=index, indicatoron=0)
            button.grid(row=index + 2)
            # sets pencile as default
            if index == 0:
                self.radio_val.set(1)
                button.invoke()
            index += 1

        # button that indicates chosen color
        self.color_button = Button(toolbox_frame, activebackground=self.color, bg=self.color, command=self.set_color)
        self.color_button.config(height=3, width=5)
        self.color_button.grid(row=6)

    def init_similarity_toolbar(self, right_frame):
        similarity_frame = Frame(right_frame)
        similarity_frame.pack(side=TOP)

        # button that calls action show similar images
        self.find_similar_flags_button = Button(similarity_frame, text="Execute", command=self.show_similar_flags)
        # scale that indicates number of similar images
        self.number_of_flags = Scale(similarity_frame, from_=1, to=10, orient=HORIZONTAL)
        # label with the text
        self.flag_label = Label(similarity_frame, text="Number of similar pictures: ")

        # pack them horizontaly
        self.flag_label.pack(side=LEFT)
        self.number_of_flags.pack(side=LEFT)
        self.find_similar_flags_button.pack(side=LEFT)

    def init_menu(self):
        """
        Function creates File and Edit menus.
        """
        # create a toplevel menu
        menubar = Menu(self.root)

        # add submenu File
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open", command=self.open_file)
        filemenu.add_command(label="Save", command=self.save_file)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        # add submenu Edit
        editmenu = Menu(menubar, tearoff=0)
        editmenu.add_command(label="Undo", command=self.undo_object)
        editmenu.add_command(label="Redo", command=self.redo_object)
        menubar.add_cascade(label="Edit", menu=editmenu)

        # display the menu
        self.root.config(menu=menubar)

    def set_color(self):
        """
        Function sets private variable self.color to chosen color from Color picker. If no color is selected then
        nothing changes.
        """
        chose_color = askcolor(initialcolor=self.color)[1]
        self.color = chose_color if chose_color is not None else self.color
        self.color_button.configure(bg=self.color, activebackground=self.color)

    def init_canvas(self, right_frame):
        """
        Function creates canvas label and binds events to canvas and main window.
        """
        canvas_frame = Frame(right_frame)
        canvas_frame.pack(side=BOTTOM)

        self.canvas = Label(canvas_frame)
        self.update_canvas_img()

        self.canvas.pack()
        self.canvas.bind("<Button-1>", self.mouse_pressed)
        self.canvas.bind("<B1-Motion>", self.mouse_moved)
        self.canvas.bind("<ButtonRelease-1>", self.mouse_released)

        self.root.bind("<Control-z>", self.undo_object)
        self.root.bind("<Control-Z>", self.redo_object)

        self.root.bind("<Control-s>", self.save_file)

    def update_canvas_img(self):
        """
        Functions sets self.opencv_image as canvas label image
        """
        img = Image.fromarray(self.opencv_image)
        self.canvas_image = ImageTk.PhotoImage(image=img)
        self.canvas.configure(image=self.canvas_image)

    def create_pen(self):
        """
        Creates pen as current object for drawing
        """
        # pop last element if it is empty
        if self.objects_to_draw.__len__() != 0 and self.objects_to_draw[-1].is_empty():
            self.objects_to_draw.pop()

        self.current_object = Pen()
        self.last_create_function_called = self.create_pen

    def create_fill(self):
        """
        Creates fill as current object for drawing
        """
        # pop last element if it is empty
        if self.objects_to_draw.__len__() != 0 and self.objects_to_draw[-1].is_empty():
            self.objects_to_draw.pop()

        self.current_object = Fill()
        self.last_create_function_called = self.create_fill

    def create_circle(self):
        """
        Creates circle as current object for drawing
        """
        # pop last element if it is empty
        if self.objects_to_draw.__len__() != 0 and self.objects_to_draw[-1].is_empty():
            self.objects_to_draw.pop()

        self.current_object = Circle()
        self.last_create_function_called = self.create_circle

    def create_line(self):
        """
        Creates line as current object for drawing
        """
        # pop last element if it is empty
        if self.objects_to_draw.__len__() != 0 and self.objects_to_draw[-1].is_empty():
            self.objects_to_draw.pop()

        self.current_object = Line()
        self.last_create_function_called = self.create_line

    def last_create_function_called(self):
        """
        Function that is called after mouse is released. Creates same object for drawing as previous one.
        """
        pass

    def mouse_pressed(self, event):
        """
        Function is called when mouse is pressed. Update methon on current object is called
        and copied image for live drawing is created.
        :param event: tkinter event
        """
        self.current_object.update(event.x, event.y, self.color, self.thickness.get())
        self.prev_opencvimg = self.opencv_image

    def mouse_moved(self, event):
        """
        Function is called when mouse i moved and pressed. It updates current object and draws it on copied image which
        imitates live drawing.
        :param event: tkinter event
        """
        self.opencv_image = self.prev_opencvimg.copy()
        # update
        self.current_object.update(event.x, event.y, self.color, self.thickness.get())
        # draw new object
        self.current_object.draw_on_img(self.opencv_image)
        # display changes
        self.update_canvas_img()

    def mouse_released(self, event):
        """
        Function is called when mouse is released and drawing for the current object is finished.
        :param event: tkinter event
        """
        # update
        self.current_object.update(event.x, event.y, self.color, self.thickness.get())

        # draw object on canvas, flood fill uses it
        self.current_object.draw_on_img(self.opencv_image, force=True)
        self.update_canvas_img()

        self.objects_to_draw.append(self.current_object)
        # create same tool object
        self.last_create_function_called()
        self.undo_objects.clear()

    def undo_object(self, event=None):
        """
        Function removes latest drawn object from canvas.
        :param event: tkinter event
        """
        if self.objects_to_draw.__len__() == 0:
            return
        # pop object from drawn objects
        last_obj = self.objects_to_draw.pop()
        # append it to undo objects
        self.undo_objects.append(last_obj)

        # create blank image
        self.opencv_image = np.zeros((self.canvas_height, self.canvas_width, 3), np.uint8)
        self.opencv_image.fill(255)

        # draw all objects again on init image
        self.opencv_image = self.initialize_image.copy()
        for obj in self.objects_to_draw:
            obj.draw_on_img(self.opencv_image, force=True)
        # display changes
        self.update_canvas_img()

    def redo_object(self, event=None):
        """
        Function draws latest element that is in undo objects list.
        :param event:
        """
        if self.undo_objects.__len__() == 0:
            return
        # emitates queueu
        last_obj = self.undo_objects.pop()
        # draw on image
        last_obj.draw_on_img(self.opencv_image, force=True)
        # display changes
        self.update_canvas_img()
        # append in objects on canvas list
        self.objects_to_draw.append(last_obj)

    def open_file(self, event=None):
        """
        Function opens file picture and sets it as opencv_img
        :param event: tkinter event because of binding
        """
        Tk().withdraw()
        filename = askopenfilename()
        if filename.__len__() == 0:
            return
        # read image from file
        self.opencv_image = cv2.imread(filename)
        # transfer it from bgr to rgb
        bgr_img = self.opencv_image.copy()
        cv2.cvtColor(src=bgr_img, dst=self.opencv_image, code=cv2.COLOR_BGR2RGB)
        # display changes
        self.update_canvas_img()
        # sets init image to this, because of undo object method
        self.initialize_image = self.opencv_image.copy()

        # clear all object data
        self.objects_to_draw.clear()
        self.undo_objects.clear()

    def save_file(self, event=None):
        """
        Saves image as file
        :param event:
        """
        Tk().withdraw()
        filename = asksaveasfilename()
        if filename.__len__() == 0:
            return
        # if exension is not provided, add "jpeg" as extension
        if not filename.lower().endswith(('.png', '.jpg', '.jpeg')):
            filename += ".jpeg"
        # convert it from rgb to bgr
        bgr_img = self.opencv_image.copy()
        cv2.cvtColor(src=self.opencv_image, dst=bgr_img, code=cv2.COLOR_RGB2BGR)
        # save image
        cv2.imwrite(filename, bgr_img)

    def find_similar_flags(self, N):
        canvasHist = self.descriptor.calculate_descriptor(self.opencv_image)
        self.flag_window_names = self.search.searchKNN(canvasHist, N)
        return self.represents.get_flags_for_names(self.flag_window_names)

    def update_flag_window(self):
        """
        Functions sets given flag:image as canvas label image on flag_window
        """
        index_flag = self.flag_window_counter % len(self.flag_window_flags)
        # Update canvas image
        img = Image.fromarray(self.flag_window_flags[index_flag])
        self.flag_window_canvas_image = ImageTk.PhotoImage(image=img)
        self.flag_window_canvas.configure(image=self.flag_window_canvas_image)

        # Update label text to country name
        self.flag_window_label.configure(text=str(index_flag + 1) + " " + self.flag_window_names[index_flag],
                                         font="-weight bold")

    def show_similar_flags(self):
        # get number of images to find
        N = self.number_of_flags.get()
        # get similar images
        self.flag_window_flags = self.find_similar_flags(N)
        self.flag_window_counter = 0

        # destroy previous window
        try:
            self.flag_window.destroy()
        except AttributeError:
            pass
        # initialize flag window
        self.init_flag_window()

    def init_flag_window(self):
        self.flag_window = Toplevel(self.root)
        self.flag_window.wm_title("Similar flags")

        # create and pack top frame
        top_frame = Frame(self.flag_window)
        top_frame.pack(side=TOP)

        # create and pack bottom frame
        bottom_frame = Frame(self.flag_window)
        bottom_frame.pack(side=BOTTOM)

        # set next and previous button icons
        self.flag_window_next_icon = PhotoImage(file=os.path.join(self.project_dir, "icons/next_icon.png"))
        self.flag_window_prev_icon = PhotoImage(file=os.path.join(self.project_dir, "icons/prev_icon.png"))

        # create buttons next and previous
        flag_window_next = Button(top_frame, image=self.flag_window_next_icon, command=self.show_next_flag)
        flag_window_prev = Button(top_frame, image=self.flag_window_prev_icon, command=self.show_prev_flag)
        flag_window_label = Label(top_frame, text="Flag of ")

        # bind key arrows functions
        self.flag_window.bind("<Left>", self.show_prev_flag)
        self.flag_window.bind("<Right>", self.show_next_flag)

        # create label for flag name
        self.flag_window_label = Label(top_frame)

        flag_window_label.grid(row=0, column=0)
        self.flag_window_label.grid(row=0, column=1)
        # pack buttons in top frame
        flag_window_prev.grid(row=0, column=2)
        # pack label in top frame
        flag_window_next.grid(row=0, column=3)
        # craete label that represents canvas
        self.flag_window_canvas = Label(bottom_frame)
        self.flag_window_canvas.pack()

        # set first image from array as canvas image on flag_window
        self.update_flag_window()

    def show_next_flag(self, event=None):
        self.flag_window_counter += 1
        self.update_flag_window()

    def show_prev_flag(self, event=None):
        self.flag_window_counter -= 1
        self.update_flag_window()


def init_parser():
    """
    Function initializes argument parser for command line call.
    :return: returns source path and destination path
    """
    parser = argparse.ArgumentParser(description='GUI for drawing flags and finding the most similar ones')
    parser.add_argument('flag_db', metavar='Flags database',
                        help='Path to the flags database.')
    parser.add_argument('hist_db', metavar='Histogram database',
                        help='Path to the histogram database.')
    args = parser.parse_args()
    return args.flag_db, args.hist_db


if __name__ == '__main__':
    flag_db, hist_db = init_parser()

    gui = GUI(flag_db, hist_db)
    gui.start()
