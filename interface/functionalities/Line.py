import  cv2
import  struct

class Line:

    def __init__(self):
        self.start_x = self.start_y = self.end_x = self.end_y = None
        self.drawn_obj = None
        self.color = None
        self.rgb_color = None
        self.width = None

    def update(self, x, y, color, width):
        """
        Updates private parameters
        :param x: x mouse coordinate
        :param y: y mouse coordinate
        :param color: current selected color
        :param thickness: current selected thickness
        :return:
        """
        #sets line start
        if self.start_x is None and self.start_y is None:
            self.start_x = x
            self.start_y = y
            #conver hex color to rgb color
            self.rgb_color = struct.unpack("BBB", bytes.fromhex(color[1:]))
            self.width = width
            return
        self.end_x = x
        self.end_y = y

    def draw_on_img(self, img, force=False):
        cv2.line(img, (self.start_x, self.start_y), (self.end_x, self.end_y), self.rgb_color, thickness=self.width)

    def is_empty(self):
        return self.end_y is None and self.end_x is None