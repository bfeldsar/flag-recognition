import math
import cv2
import struct

class Circle:

    def __init__(self):
        self.center_x = self.center_y = None
        self.radius = None
        self.drawn_obj = None
        self.rgb_color = None
        self.rgb_color = None
        self.width = None

    def update(self, x, y, color, width):
        """
        Updates private parameters
        :param x: x mouse coordinate
        :param y: y mouse coordinate
        :param color: current selected color
        :param thickness: current selected thickness
        :return:
        """
        # sets circle center
        if self.center_x is None and self.center_y is None:
            self.center_x = x
            self.center_y = y
            #conver hex color to rgb color
            self.rgb_color = struct.unpack("BBB", bytes.fromhex(color[1:]))
            self.width = width
            return
        #calculate radius
        self.radius = math.hypot(x - self.center_x, y - self.center_y)

    def draw_on_img(self, img, force=False):
        cv2.circle(img, (self.center_x, self.center_y), int(self.radius), self.rgb_color, self.width)

    def is_empty(self):
        return self.radius is None