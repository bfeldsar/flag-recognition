import cv2
import struct

class Pen:

    def __init__(self):
        self.drawn_obj = None
        self.points = []
        self.color = None
        self.rgb_color = None
        self.width = None

    def update(self, x, y, color, width):
        """
        Updates private parameters
        :param x: x mouse coordinate
        :param y: y mouse coordinate
        :param color: current selected color
        :param thickness: current selected thickness
        :return:
        """
        self.points.append((x,y))
        if self.width is None:
            #conver hex color to rgb color
            self.rgb_color = struct.unpack("BBB", bytes.fromhex(color[1:]))
            self.width = width

    def draw_on_img(self, img, force = False):
        prev_tup = None
        for tup in self.points:
            if prev_tup is None:
                prev_tup = tup
                continue
            cv2.line(img, prev_tup, tup, self.rgb_color, thickness=self.width)
            prev_tup = tup

    def is_empty(self):
        return self.points.__len__() == 2