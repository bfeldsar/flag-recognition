import cv2
import numpy as np
import struct


class Fill:
    def __init__(self):
        self.seed_x = None
        self.seed_y = None
        self.replacement_rgb_color = None
        self.lo = 179
        self.hi = 187
        self.called = False

    def update(self, x, y, color, thickness):
        """
        Updates private parameters
        :param x: x mouse coordinate
        :param y: y mouse coordinate
        :param color: current selected color
        :param thickness: current selected thickness
        :return:
        """
        if self.seed_x is None and self.seed_y is None:
            self.seed_x = x
            self.seed_y = y
            #conver hex color to rgb color
            self.replacement_rgb_color = struct.unpack("BBB", bytes.fromhex(color[1:]))

    def draw_on_img(self, img, force = False):
        #if method is allready called and it is not forced to draw return
        if not force and self.called:
            return

        self.called = True
        h, w = img.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)
        cv2.floodFill(img, mask, (self.seed_x, self.seed_y), self.replacement_rgb_color, (self.lo,) * 3, (self.hi,) * 3,
                      cv2.FLOODFILL_FIXED_RANGE)

    def is_empty(self):
        return self.seed_y is None and self.seed_x is None