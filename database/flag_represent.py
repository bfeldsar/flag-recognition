from os import listdir
from os import path as path
from scipy import misc

import time, re

class FlagRepresents:

    def __init__(self, dataset_path):
        self.represents = {}
        self.load_dataset_represents(dataset_path)

    def load_dataset_represents(self, dataset_path):
        start = time.clock()
        for dir_name in listdir(dataset_path):
            dir_path = path.join(dataset_path,dir_name)
            #means that first image in directory is representive one
            represent_file = [file for file in listdir(dir_path) if re.search(r'.*4.*',file)][0]
            #load image
            self.represents[dir_name] = misc.imread(path.join(dir_path, represent_file))

        end = time.clock()
        print("Represents loaded in :", end - start, " sec")

    def get_flags_for_names(self, names):
       return [self.represents[name] for name in names]