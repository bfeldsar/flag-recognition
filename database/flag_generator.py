from wand.image import Image
from os import listdir
from os import makedirs
from more_itertools import unique_everseen

import os.path as path
import argparse, time

# max transform width of orignal picture
max_width = 600
# framework width
frame_width = 500
# framework height
frame_height = 300
# step for generating pictures
step = 25


def main():
    # get source and destination path
    src_path, dest_path = init_parser()
    # lsit of svg pics names
    only_svg_pic = []

    # add only svg pictures in list
    for file_name in listdir(src_path):
        file_path = path.join(src_path, file_name)

        # check if it is file and it extension is svg
        if path.isfile(file_path) and path.splitext(file_name)[1] == ".svg":
            only_svg_pic.append((file_path, path.splitext(file_name)[0]))

    generate_databse(only_svg_pic, dest_path)


def generate_databse(svg_pic, dest_path):
    """
    Generates database from svg pictures and stores it in destination path
    :param svg_pic: list of svg pictures, ( full path, file name)
    :param dest_path: full destitanion path for storing database
    :return:
    """
    pics_num = len(svg_pic)
    # generate
    for index, (pic_path, pic_name) in enumerate(svg_pic):
        # create folder with flag name
        dest_folder = path.join(dest_path, pic_name)
        makedirs(dest_folder)
        # print progress
        print("Flag " + str(index + 1) + "/" + str(pics_num))

        for pic_width in range(frame_width, max_width + step, step):
            generate_pictures(pic_path, pic_name, pic_width, dest_folder)


def init_parser():
    """
    Function initializes argument parser for command line call.
    :return: returns source path and destination path
    """
    parser = argparse.ArgumentParser(description='Flag database creation')
    parser.add_argument('src', metavar='Source Path',
                        help='Path to the directory which contains the svg pictures of flags.')
    parser.add_argument('dest', metavar='Destination Path',
                        help='Path to the directory where database will be created.')
    args = parser.parse_args()
    return args.src, args.dest


def generate_pictures(pic_path, pic_name, resize_width, dest_folder):
    """
    Function resize original picture, then zoom it and move framework in corners and in the middle of picture. Framework's
    dimension is min_width and min_height.

    :param pic_path: Full path to the picture
    :param pic_name: Picture name
    :param resize_width: Width on which original picture will be resized. Aspect ratio of picture is preserved.
    :param dest_folder: Folder where generated pictures will be saved
    :return:
    """
    with Image(filename=pic_path) as img:
        # transform original picture
        img.transform(resize=str(resize_width))
        img.format = 'jpeg'
        # save coordinates of the corners and middle of the resized picture
        COORDINATES = [
            (0, 0),
            (resize_width - frame_width, 0),
            (0, img.height - frame_height),
            (resize_width - frame_width, img.height - frame_height),
            (int((resize_width - frame_width)) / 2, int((img.height - frame_height) / 2))
        ]

        print("Generating " + pic_name + " in initial width " + str(resize_width))

        for index, (x_start, y_start) in enumerate(unique_everseen(COORDINATES)):
            # copy image
            with img.clone() as crop_img:
                crop_img.format = "jpeg"
                # crop image with given starting coordinates
                crop_img.transform(
                    crop=str(frame_width) + "x" + str(frame_height) + "+" + str(x_start) + "+" + str(y_start) + "")
                crop_img.resize(frame_width, frame_height)
                # filename example: Croatia0_575
                crop_img.save(
                    filename=path.join(dest_folder, pic_name + str(index) + "_" + str(resize_width) + ".jpeg"))


start = time.clock()
main()
end = time.clock()
print("Database generated in: ", end - start, "sec")
